import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class SimpleForm extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// -----------------------------------------------
		// 1. Create & configure user interface controls
		// -----------------------------------------------

		// name label
		Label nameLabel = new Label("Enter your name");

		// name textbox
		TextField nameTextBox = new TextField();

		// button
		Button goButton = new Button();
		goButton.setText("Click me!");
		// Add button click handler
		goButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		        // Logic for what should happen when you push button
		    	//1.when person presses button , output HELLO +'name'
		    	String name=nameTextBox.getText();
		    	System.out.println("HELLO "+name);
		    	
		    	// Clear Text Box
		    	nameTextBox.setText("");
		    }
		});
		// -----------------------------------------------
		// 2. Make a layout manager
		// -----------------------------------------------
		VBox root = new VBox();
		root.setSpacing(15);
		// -----------------------------------------------
		// 3. Add controls to the layout manager
		// -----------------------------------------------

		// add controls in the same order you want them to appear
		root.getChildren().add(nameLabel);
		root.getChildren().add(nameTextBox);
		root.getChildren().add(goButton);

		// -----------------------------------------------
		// 4. Add layout manager to scene
		// 5. Add scene to a stage
		// -----------------------------------------------

		// set the height & width of app to (300 x 250);
		primaryStage.setScene(new Scene(root, 300, 250));

		// setting the title bar of the app
		primaryStage.setTitle("Example 01");

		// -----------------------------------------------
		// 6. Show the app
		// -----------------------------------------------
		primaryStage.show();

	}

}
